# KiCad Symbols

This repository contains the official KiCad schematic symbol libraries.

**The libraries in this repositiory are intended for KiCad version 6.x**

Each symbol library is stored as a `.kicad_sym` file.

Contribution guidelines can be found at http://kicad-pcb.org/libraries/contribute
The library convention can be found at http://kicad-pcb.org/libraries/klc/

Other KiCad library repositories are located:

* Footprints: https://gitlab.com/kicad/libraries/kicad-footprints
* 3D Models: https://gitlab.com/kicad/libraries/kicad-packages3d
* Templates: https://gitlab.com/kicad/libraries/kicad-templates
